
import * as types from './types';

const INITIAL_STATE = {
  isFetching: false,
  error: null,
  data: null
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.<%= name.toUpperCase() %>_REQUEST:
      return { ...state, isFetching: true, error: null, data: null };
    case types.<%= name.toUpperCase() %>_FAILURE:
      return { ...state, isFetching: false, error: action.payload, data: null };
    case types.<%= name.toUpperCase() %>_SUCCESS:
      return { ...state, isFetching: false, error: null, data: action.payload };
    default:
      return state;
  }
}