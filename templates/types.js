export const <%= name.toUpperCase() %>_REQUEST = '<%= name %>_request';
export const <%= name.toUpperCase() %>_SUCCESS = '<%= name %>_success';
export const <%= name.toUpperCase() %>_FAILURE = '<%= name %>_failure';