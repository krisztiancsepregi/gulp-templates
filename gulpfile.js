
const argv = require('yargs').argv;
const gulp = require('gulp');
const template = require('gulp-template');

gulp.task('createFeature', () => {
  const { name } = argv;

  if (name && name.length > 0) {
    const destinationPath = `src/store/${name}`;

    gulp.src('templates/actions.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/reducers.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/types.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    return;
  }

  console.log('******************************************');
  console.log('* ERROR: You must provide a feature name *');
  console.log('* HINT:  gulp createFeature --name login *');
  console.log('******************************************');
});